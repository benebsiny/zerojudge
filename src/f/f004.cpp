#include <stdio.h>
#define min(a, b) ((a)<(b)?(a):(b))
int main(){
    int arr[7] = {1000, 500, 100, 50, 10, 5, 1};
    int num;
    while(~scanf("%d", &num)){
        printf("%d = ", num);
        for(int i=0; i<7; i++){
            if(num/arr[i]){
                printf("%d*%d", arr[i], num/arr[i]);
                num %= arr[i];
                if(num) printf(" + ");
            }
        }
        printf("\n");
    }
    return 0;
}