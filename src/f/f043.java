package f;

import java.util.Arrays;
import java.util.Scanner;

public class f043 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int[] arr = new int[3];
        arr[0] = sca.nextInt();
        arr[1] = sca.nextInt();
        if (arr[0] == arr[1]) {
            arr[0] -= 3;
        }
        arr[2] = Math.abs(arr[0]-arr[1]);
        Arrays.sort(arr);

        System.out.printf("%d+%d=%d%n", arr[0], arr[1], arr[2]);
    }
}
