package f;

import java.util.*;
import java.util.Map.Entry;

public class f149 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int row = sca.nextInt();
        int col = sca.nextInt();

        Map<Pos, ArrayList<Pos>> dict = new HashMap<>();

        int[][] map = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                map[i][j] = sca.nextInt();
            }
        }

        // 把八個方位的位置用陣列的方式表示出來
        int[][] dir = {
                {-1, -1}, {-1, 0}, {-1, 1},
                {0, -1}, {0, 1},
                {1, -1}, {1, 0}, {1, 1}};
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (map[i][j] == 5) {
                    for (int d = 0; d < 8; d++) {
                        if (0 <= i + dir[d][0] && i + dir[d][0] < row &&
                                0 <= j + dir[d][1] && j + dir[d][1] < col) {
                            // 如果找到地雷的話
                            if (map[i + dir[d][0]][j + dir[d][1]] == 1) {
                                ArrayList<Pos> pos = dict.get(new Pos(i, j));
                                // 看看map有沒有該位置的key，沒的話就新增一個map，有的話就在map的value再加一個
                                if (pos == null) {
                                    ArrayList<Pos> newPos = new ArrayList<>();
                                    newPos.add(new Pos(i + dir[d][0], j + dir[d][1]));
                                    dict.put(new Pos(i, j), newPos);
                                } else {
                                    pos.add(new Pos(i + dir[d][0], j + dir[d][1]));
                                }
                            }
                            // 如果找到地雷偵測器的話
                            else if (map[i + dir[d][0]][j + dir[d][1]] == 5) {
                                ArrayList<Pos> pos = dict.get(new Pos(i, j));
                                if (pos != null) { // 就把這個位置所找到的地雷給清除掉
                                    dict.remove(new Pos(i, j));
                                }
                                break; // 不用再繼續找下去了
                            }
                        }
                    }
                }
            }
        }
        Set<Entry<Pos, ArrayList<Pos>>> entries = dict.entrySet();
        for (Entry entry : entries) {
            ArrayList<Pos> posArrayList = (ArrayList<Pos>) entry.getValue();
            for (Pos pos : posArrayList) {
                map[pos.i][pos.j] = 2; // 把有被地雷偵測器偵測到的地雷標爲2
            }
        }

        int find = 0, cantFind = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (map[i][j] == 1) {
                    cantFind++; // 如果該位置還是1的話，那就是沒被地雷偵測器找到
                } else if (map[i][j] == 2) {
                    find++;
                }
            }
        }
        System.out.println(find + " " + cantFind);
    }

     static class Pos {
        public int i;
        public int j;

        public Pos(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object obj) {
            Pos other = (Pos) obj;
            return this.i == other.i && this.j == other.j;
        }

        @Override
        public int hashCode() {
            return i * 100 + j;
        }
    }
}