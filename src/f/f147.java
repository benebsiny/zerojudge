package f;

import java.util.Scanner;

public class f147 {
    public static void main(String[] args) {
        Food[] pack = {
                new Food("Medium Wac", 4),
                new Food("WChicken Nugget", 8),
                new Food("Geez Burger", 7),
                new Food("ButtMilk Crispy Chicken", 6),
                new Food("Plastic Toy", 3),
        };
        Food[] single = {
                new Food("German Fries", 2),
                new Food("Durian Slices", 3),
                new Food("WcFurry", 5),
                new Food("Chocolate Sunday", 7)
        };

        Scanner sca = new Scanner(System.in);
        int total = 0;
        while (true) {
            int cmd = sca.nextInt();
            if (cmd == 0) {
                break;
            }
            if (cmd == 1) {
                int order = sca.nextInt() - 1;
                total += pack[order].price;
                System.out.println(pack[order].name + " " + pack[order].price);
            } else if (cmd == 2) {
                int order = sca.nextInt() - 1;
                total += single[order].price;
                System.out.println(single[order].name + " " + single[order].price);
            }
        }
        System.out.println("Total: " + total);
    }

    static class Food {
        String name;
        int price;

        public Food(String name, int price) {
            this.name = name;
            this.price = price;
        }
    }
}
