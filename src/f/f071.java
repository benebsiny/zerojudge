package f;

import java.util.Scanner;

public class f071 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int[] luckyNums = {sca.nextInt(),
                sca.nextInt(), sca.nextInt()};
        int[] nums = {
             sca.nextInt(), sca.nextInt(), sca.nextInt(),
                sca.nextInt(), sca.nextInt()};
        int[] money = {
             sca.nextInt(), sca.nextInt(), sca.nextInt(),
                sca.nextInt(), sca.nextInt()};
        int total = 0;
        for(int i=0; i<2; i++){
            for(int j=0; j<5; j++){
                if(luckyNums[i]==nums[j]){
                    total += money[j];
                }
            }
        }
        boolean third = false;
        for(int j=0; j<5; j++){
            if(luckyNums[2]==nums[j]){
                total -= money[j];
                third = true;
            }
        }
        if(total<0){
            total = 0;
        }
        if(!third){
            total *= 2;
        }
        System.out.println(total);
    }
}
