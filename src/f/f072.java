package f;

import java.util.Scanner;

public class f072 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();

        int[] garden = new int[count];

        for (int i = 0; i < count; i++) {
            garden[i] = sca.nextInt();
        }

        int first = 0, last = 0;
        for (int i = 0; i < count; i++) {
            if (garden[i] == 1) {
                first = i;
                break;
            }
        }
        for (int i = count - 1; i >= 0; i--) {
            if (garden[i] == 1) {
                last = i;
                break;
            }
        }
        int flower = 0;
        for (int i = first + 1; i < last; i++) {
            if (garden[i - 1] != 9 && garden[i + 1] != 9 && garden[i] == 0) {
                flower++;
            }
        }
        System.out.println(flower);
    }
}
