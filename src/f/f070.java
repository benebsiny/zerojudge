package f;

import java.util.Scanner;

public class f070 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int[][] nums = new int[3][2];

        for (int i = 0; i < 3; i++) {
            for (int j = 1; j >= 0; j--) {
                nums[i][j] = sca.nextInt();
            }
        }

        int mulAll = nums[0][1] * nums[1][1] * nums[2][1];
        long sum = 0; // 記得用long，不然會溢位
        for (int i = 0; i < 3; i++) {
            int r1 = nums[i][1], r2 = mulAll / nums[i][1];
            int q, r;
            int t1 = 0, t2 = 1, t;
            while (r2 != 0) {
                q = r1 / r2;
                r = r1 % r2;

                t = t1 - t2 * q;

                r1 = r2;
                r2 = r;

                t1 = t2;
                t2 = t;
            }
            while (t1 < 0) {
                t1 += nums[i][1];
            }
            sum += (long)t1 * (long)nums[i][0] * (long)(mulAll / nums[i][1]);
        }
        System.out.println(sum % mulAll);
    }
}
