package f;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class f148 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int row = sca.nextInt(), col = sca.nextInt();
        int count = sca.nextInt();

        Map<String, Pos> map = new TreeMap<>();
        for (int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                String input = sca.next();
                if(!input.equals("0")){
                    map.put(input, new Pos(i, j));
                }
            }
        }
        if(map.size()<count){
            System.out.println("Mission fail.");
        }
        else {
            Set<Map.Entry<String, Pos>> sets = map.entrySet();
            int index = 0;
            for(Map.Entry<String, Pos> entry: sets){
                System.out.println(entry.getValue().i + " " + entry.getValue().j);
                index++;
                if(index>=count)
                    break;
            }
        }
    }

    static class Pos {
        public int i;
        public int j;
        Pos(int i, int j){
            this.i = i;
            this.j = j;
        }
    }
}

