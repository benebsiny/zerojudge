package f;

import java.util.Arrays;
import java.util.Scanner;

public class f045 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        String str = sca.nextLine();
        char[] arr = str.toCharArray();
        Arrays.sort(arr);

        int max = (arr[arr.length - 1] - '0') * (arr[arr.length - 1] - '0')
                + (arr[arr.length - 2] - '0') * (arr[arr.length - 2] - '0');

        if ((str.charAt(str.length() - 3) - '0') * 100 + (str.charAt(str.length() - 2) - '0') * 10 +
                (str.charAt(str.length() - 1) - '0') == max)
            System.out.println("Good Morning!");
        else
            System.out.println("SPY!");
    }
}
