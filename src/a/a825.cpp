#include <iostream>
#include <algorithm>

using namespace std;
int main(){
    int count;
    cin >> count;
    int arr[count];
    for(int i=0; i<count; i++){
        cin >> arr[i];
    }
    sort(arr, arr+count);
    reverse(arr, arr+count);

    int a = 0, b = 0;
    for(int i=0; i<count; i++){
        if(a < b){
            a *= 10;
            a += arr[i];
        } else {
            b *= 10;
            b += arr[i];
        }
    }
    cout << a * b;

    
    return 0;
}