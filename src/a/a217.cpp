#include <iostream>
#include <string>
using namespace std;
int main(){
    string str;
    while(getline(cin, str)){
        bool turn = true;
        for(int i=0; i<str.length(); i++){
            if(str[i]=='i'){
                if(str.length()==1){
                    str[i] = 'I';
                    turn = false;
                } else if(i==0 && !isalpha(str[i+1])){
                    str[i] = 'I';
                    turn = false;
                } else if(i==str.length()-1 && !isalpha(str[i-1])){
                    str[i] = 'I';
                    turn = false;
                } else if(i!=0 && i!=str.length()-1 &&
                     !isalpha(str[i-1]) && !isalpha(str[i+1])) {
                    str[i] = 'I';
                    turn = false;
                }
            }
            if(isalpha(str[i]) && turn){
                str[i] = toupper(str[i]);
                turn = false;
            }
            if(!isalpha(str[i]) && str[i]!=' ' && str[i]!=','){
                turn = true;
            }
        }
        cout << str << '\n';
    }
}