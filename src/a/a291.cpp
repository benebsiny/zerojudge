#include <stdio.h>

int main(){
    int arr[4];
    int input[4];
    int a, b;
    int count;
    while(~scanf("%d%d%d%d", &arr[0], &arr[1], &arr[2], &arr[3])){
        scanf("%d", &count);
        while(count--){
            bool found[4] = {};
            a = 0, b = 0;
            scanf("%d%d%d%d", &input[0], &input[1], &input[2], &input[3]);
            for(int i=0; i<4; i++){
                if(arr[i]==input[i]){
                    a++;
                    found[i] = true;
                }
            }
            bool found2[4] = {};
            for(int i=0; i<4; i++){
                if(found[i]) continue;
                for(int j=0; j<4; j++){
                    if(found[j] || found2[j]) continue;
                    if(input[i]==arr[j]){
                        b++;
                        found2[j] = true;
                        break;
                    }
                }
            }
            printf("%dA%dB\n", a, b);
        }
    }

    return 0;
}