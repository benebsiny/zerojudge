#include <iostream>
#include <vector>
#include <string>
using namespace std;

int needToCut(string str){
    for(int i=0; i<str.length(); i++){
        if(!('A' <= str[i] && str[i] <= 'Z') && !('a' <= str[i] && str[i] <= 'z')){
            return i;
        }
    }
    return -1;
}
bool hasDiff(string str, string k){
    if (str.length() > k.length()){
		return false;
    }
	int index = 0;
	for (int i = 0; i != str.length(); i++) {
		while (k.length() != index && str[i] != k[index]){
            index++;
        }
		if (k.length() == index){
			return false;
        }
	}
	return true;
}
string findIt(string str, vector<string> dict){
    if(str.length()==0){
        return "";
    }
    string found;
    int min = __INT_MAX__, c = -1, r = 0;
    for(string k: dict){
        if(hasDiff(str, k)){
            r = k.length() - str.length();
            if(r < min){
                min = r;
                c = 1;
                found = k;
            } else if(r==min){
                c++;
            }
        }
    }
    if(c!=1){
        return str;
    } else {
        return found;
    }
}
int main(){
    vector<string> dict;
    
    string str;
    while((cin >> str) && str != "|"){
        dict.push_back(str);
    }
    int min = __INT_MAX__, c = 1;
    string found;
    while(cin >> str){
        int index = needToCut(str);
        if(index!=-1){
            cout << findIt(str.substr(0, index), dict) << str[index];
            cout << findIt(str.substr(index+1, str.length()), dict) << ' ';
        }
        else {
            cout << findIt(str, dict) << ' ';
        }
    }

    return 0;
}