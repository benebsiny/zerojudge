#include <stdio.h>
#include <set>
#include <algorithm>
using namespace std;
int main(){
    set<int> s;
    set<int>::iterator up, low;

    int count, num, sum;
    scanf("%d", &count);

    scanf("%d", &num);
    s.insert(num);
    sum = num;
    
    for(int i=1; i<count; i++){
        scanf("%d", &num);
        low = s.lower_bound(num);
        up = low;
        if(low!=s.begin()) // less than current num
            low--;

        if(up==s.end()) // not found
            sum += num - *low;
        else // upper vs lower
            sum += min(abs(*up - num), abs(num - *low));

        s.insert(num);
    }
    printf("%d\n", sum);
    return 0;
}