#include <iostream>
#include <sstream>
#include <vector>
using namespace std;
int GCD(int a, int b){
    if(b) while((a%=b) && (b%=a));
    return a + b;
}
int main(){
    int count, num;
    stringstream ss;
    string line;
    cin >> count >> ws;
    while(count--){
        vector<int> vec;
        getline(cin, line);
        ss << line;
        while (ss >> num) {
            vec.push_back(num);
        }

        int MAX = 0;
        for(int i=0; i<vec.size()-1; i++){
            for(int j=i+1; j<vec.size(); j++){
                MAX = max(MAX, GCD(vec[i], vec[j]));
            }
        }
        cout << MAX << '\n';
        ss.clear();
    }
    return 0;
}