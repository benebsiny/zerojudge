#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int main(){
    cin.sync_with_stdio(0), cin.tie(nullptr);
    string str;
    int mx = 0, cur = 0;
    while(getline(cin, str)){
        cur = mx = 0;
        for(int i=0; i<str.length(); i++){
            if(str[i] == ' '){
                cur++;
            } else {
                mx = max(mx, cur);
                cur = 0;
            }
        }
        cout << (int)ceil(log2(mx)) << '\n';
    }
    return 0;
}