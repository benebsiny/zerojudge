#include <iostream>
#include <string>
using namespace std;
int main(){
    ios_base::sync_with_stdio(false);
    string str;
    while(true){
        cin >> str;
        if(!str.compare(".")){
            break;
        }
        if(str.length()==1){
            cout << 1 << endl;
            continue;
        }
        int failure[str.length()] = {};
        failure[0] = -1;
        for (int j = 1; j < str.length(); j++){
            int i = failure[j - 1];
            while((str[j]!=str[i+1])&&(i>=0)){
                i = failure[i];
            }

            if(str[j]==str[i+1]){
                failure[j] = i + 1;
            }
            else{
                failure[j] = -1;
            }
        }

        int x;
        for(x=str.length()-1; x>=0; x--){
            if(failure[x]==-1){
                break;
            }
        }
        x++;
        if(x>str.length()/2){
            cout << 1 << endl;
        } else if(str.length() % x!=0){
            cout << 1 << endl;
        } else {
            cout << str.length() / x << endl;
        }
    }
    return 0;
}