#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;
int main(){
    int dom[29][2];
    int index = 1;
    for(int i=0; i<=6; i++){
        for(int j=i; j<=6; j++){
            dom[index][0] = i;
            dom[index][1] = j;
            index++;
        }
    }
    
    int num;
    string dir;
    while(cin >> num >> dir){
        vector<int> front;
        vector<int> end;
        vector<int> id;
        if(!dir.compare("F")){
            front.push_back(dom[num][0]);
            end.push_back(dom[num][1]);
        }
        else {
            front.push_back(dom[num][1]);
            end.push_back(dom[num][0]);
        }
        id.push_back(num);

        while(true){
            cin >> num >> dir;
            if(num==0 && !dir.compare("F")){
                break;
            }
            if(!dir.compare("F")){
                front.push_back(dom[num][0]);
            end.push_back(dom[num][1]);
            }
            else {
                front.push_back(dom[num][1]);
            end.push_back(dom[num][0]);
            }
            id.push_back(num);

            while(front.size()>=2 && end[end.size()-2] == front.back()){
                front.pop_back();
                front.pop_back();
                end.pop_back();
                end.pop_back();
                id.pop_back();
                id.pop_back();
            }
        }
        for(int i=0; i<id.size(); i++){
            cout << id[i] << " ";
        }
        if(id.size()==0){
            cout << "DATASET CLEARED";
        }
        cout << endl;
    }
}