#include <iostream>
using namespace std;

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(){
    int count;
    while(cin >> count){
        int arr[count];
        int ops = 0;
        for(int i=0; i<count; i++){
            cin >> arr[i];
            for(int j=i; j>=1; j--){
                if(arr[j-1] > arr[j]){
                    swap(&arr[j-1], &arr[j]);
                    ops++;
                } else {
                    break;
                }
            }
        }
        cout << "Minimum exchange operations : " << ops << '\n';
    }
    return 0;
}