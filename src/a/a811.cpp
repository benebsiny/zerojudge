#include <iostream>
#include <algorithm>
typedef long long ll;
using namespace std;

ll GCD(ll a, ll b){
    if(b) while((a%=b) && (b%=a));
    return a+b;
}

int main(){
    int count, duckCount;
    cin >> count;
    while(count--){
        cin >> duckCount;
        ll arr[duckCount];
        for(int i=0; i<duckCount; i++){
            cin >> arr[i];
        }
        sort(arr, arr+duckCount);

        ll gcd = arr[1] - arr[0];
        for(int i=2; i<duckCount; i++){
            gcd = GCD(gcd, arr[i]-arr[i-1]);
        }
        cout << gcd << '\n';
    }
    return 0;
}