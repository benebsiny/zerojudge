#include <iostream>
using namespace std;

int main(){
    int a, b, c;
    cin >> a >> b >> c;
    int sum = 0;
    for(int i=1; i<=c; i++){
        if(!(i%a) || !(i%b)){
            sum += i;
        }
    }
    cout << char(sum%26+'A'-1);
    return 0;
}