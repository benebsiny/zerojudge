#include <iostream>
using namespace std;
int main(){
    int c, s, q, c1, c2, d, cases = 1;
    while((cin >> c >> s >> q)){
        if(!(c || s || q)) break;

        int arr[c+1][c+1];
        for(int i=0; i<=c; i++)
            for(int j=0; j<=c; j++)
                arr[i][j] = __INT_MAX__;
        

        for(int i=0; i<s; i++){
            cin >> c1 >> c2 >> d;
            arr[c1][c2] = d;
            arr[c2][c1] = d;
        }

        for(int k=0; k<=c; k++){
            for(int i=0; i<=c; i++){
                for(int j=0; j<=c; j++){
                    if(max(arr[i][k], arr[k][j]) < arr[i][j]){
                        arr[i][j] = max(arr[i][k], arr[k][j]);
                    }
                }
            }
        }

        cout << "Case #" << (cases++) << '\n';
        while(q--){
            cin >> c1 >> c2;
            if(arr[c1][c2]==__INT_MAX__){
                cout << "no path\n";
            } else {
                cout << arr[c1][c2] << '\n';
            }
        }
        cout << '\n';
    }
    return 0;
}