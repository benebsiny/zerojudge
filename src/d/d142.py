# 這題要開直式平方根
import math
count = int(input())
for i in range(count):
    input()
    num_str = input()
    index = 0

    ans = []

    if len(num_str) % 2 == 1:
        rest = int(num_str[:1])
        index = 1
    else:
        rest = int(num_str[:2])
        index = 2

    calc = int(math.sqrt(rest))
    ans.append(calc)

    if index >= len(num_str):
        print(calc)
        continue

    rest -= calc*ans[-1]
    rest *= 100
    rest += int(num_str[index:index+2])

    index += 2

    calc += calc
    calc *= 10

    while True:
        for digit in range(1, 11):
            if (calc + digit)*digit > rest:
                ans.append(digit-1)
                break

        rest -= (calc+ans[-1])*ans[-1]

        if index < len(num_str):
            rest *= 100
            rest += int(num_str[index:index+2])
            index += 2
        else:
            break

        calc += ans[-1] + ans[-1]
        calc *= 10

    for ele in ans:
        print(ele, end='')

    print()
