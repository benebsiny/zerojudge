#include <iostream>
#define max(a, b) ((a)>(b)?(a):(b))
using namespace std;
int main(){
    int n, k, sum = 0;
    cin >> n >> k;

    int num[n+1];
    for(int i=0; i<n; i++){
        cin >> num[i];
        sum += num[i];
    }

    int dp[sum/2+1] = {};
    for(int i=0; i<n; i++){
        for(int j=sum/2; j>=num[i]; j--){
            dp[j] = max(dp[j], dp[j-num[i]]+num[i]);
        }
    }
    cout << dp[sum/2] << " " << sum-dp[sum/2] << "\n";
    return 0;
}