package d;

import java.util.ArrayList;
import java.util.Scanner;

public class d872 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        while (sca.hasNext()) {
            int count = sca.nextInt();
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < count; i++) {
                list.add(sca.nextInt());
            }
            list.sort(Integer::compareTo);

            int res = 0;
            while (list.size() >= 4) {
                res += Math.min(list.get(0) * 2 + list.get(list.size() - 1) + list.get(list.size() - 2),
                        list.get(0) + list.get(1) * 2 + list.get(list.size() - 1));
                list.remove(list.size() - 1);
                list.remove(list.size() - 1);
            }
            if (list.size() == 3) {
                res += list.stream().mapToInt(e -> e).sum();
            } else if (list.size() == 2) {
                res += list.get(1);
            } else if (list.size() == 1) {
                res += list.get(0);
            }
            System.out.println(res);
        }
    }
}
