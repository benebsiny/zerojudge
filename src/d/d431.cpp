#include <stdio.h>

void findNum(int num, int arr[][5]){
    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            if(arr[i][j]==num){
                arr[i][j] = 0;
                return;
            }
        }
    }
}

int checked(int arr[][5]){
    int line = 0;
    for(int i=0; i<5; i++){
        if(arr[i][0]+arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4]==0){
            line++;
        }
    }

    for(int j=0; j<5; j++){
        if(arr[0][j]+arr[1][j]+arr[2][j]+arr[3][j]+arr[4][j]==0){
            line++;
        }
    }

    if(arr[0][0]+arr[1][1]+arr[2][2]+arr[3][3]+arr[4][4]==0){
        line++;
    }
    if(arr[0][4]+arr[1][3]+arr[2][2]+arr[3][1]+arr[4][0]==0){
        line++;
    }
    return line;
}

int main(){
    int a[5][5];
    int b[5][5];

    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            scanf("%d", &a[i][j]);
        }
    }
    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            scanf("%d", &b[i][j]);
        }
    }

    int num;
    int aDoneCount = 0;
    int bDoneCount = 0;

    for(int i=0; i<25; i++){
        scanf("%d", &num);
        findNum(num, a);
        aDoneCount = checked(a);

        findNum(num, b);
        bDoneCount = checked(b);

        if(aDoneCount>=5 && bDoneCount>=5){
            printf("AB %d\n", num); break;
        } else if(aDoneCount>=5){
            printf("A %d\n", num); break;
        } else if(bDoneCount>=5){
            printf("B %d\n", num); break;
        }
    }
    return 0;
}
