#include <stdio.h>
#include <algorithm>
using namespace std;
int main(){
    int coins[] = {1, 5, 10, 30, 50, 70, 100, 110};
    int count, input, res = 0;
    scanf("%d", &count);
    while(count--){
        scanf("%d", &input);
        res = input / 1000;
        input %= 1000;
        res += input / 500;
        input %= 500;
        int arr[500] = {};
        for(int i=1; i<500; i++) arr[i] = __INT_MAX__;
        for(int i=0; i<8; i++){
            for(int j=coins[i]; j<500; j++){
                arr[j] = min(arr[j], arr[j-coins[i]]+1);
            }
        }
        printf("%d\n", res + arr[input]);
    }
    return 0;
}