import sys
import math
for line in sys.stdin:
    s = line.strip('\n')
    if(s==''): continue
    num = int(s)

    sq = int(math.sqrt(num))
    calc = sq * 2
    rest = num - sq * sq
    print(int(math.sqrt(num)), ".", sep='' ,end='')
    for i in range(50):
        rest *= 100
        calc *= 10
        for j in range(1, 11):
            if (calc+j)*j > rest:
                print(j-1, end='')
                break
        
        rest -= (calc + j - 1) * (j - 1)
        calc += (j - 1) * 2
    print()