#include <iostream>
#include <functional>
#include <algorithm>

using namespace std;
int main(){
    int count, full;
    cin >> count >> full;
    int food[count];
    for(int i=0; i<count; i++){
        cin >> food[i];
    }
    sort(food, food+count, greater<int>());
    int sum = 0;
    int i;
    for(i=0; i<count; i++){
        sum += food[i];
        if(sum >= full){
            cout << i+1;
            break;
        }
    }
    if(i>=count){
        cout << "OAQ";
    }
    return 0;
}