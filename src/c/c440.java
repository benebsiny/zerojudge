package c;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class c440 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        String str = sca.nextLine();
        int total = 0;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'Q') {
                total++;
            } else if (str.charAt(i) == 'A') {
                list.add(total);
                total++;
            }
        }
        long res = 0;
        for (int i = 0; i < list.size(); i++) {
            res += (list.get(i) - i) * ((total - list.get(i) - 1) - (list.size() - i - 1));
        }
        System.out.println(res);
    }
}
