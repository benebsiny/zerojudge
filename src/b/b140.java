package b;

import java.util.Scanner;

public class b140 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int totalTime = sca.nextInt();
        int count = sca.nextInt();

        int[] dp = new int[totalTime + 1];
        while (count-- != 0) {
            int t = sca.nextInt();
            int v = sca.nextInt();
            for (int i = totalTime; i >= t; i--) {
                dp[i] = Math.max(dp[i], dp[i - t] + v);
            }
        }
        System.out.println(dp[totalTime]);
    }
}
