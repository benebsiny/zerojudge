#include<iostream>
#include <vector>
#define max(a, b) ((a)>(b)?(a):(b))
using namespace std;

int main()
{
    int n;
    while(cin >> n)
    {
        if(n==0) break;
        vector<int> vec(n), dp(n+2);
        for(int i=0; i<n; i++){
            cin >> vec[i];
        }
        for(int i=0; i<n; i++) {
            dp[i+1] = max(dp[i+1], dp[i] + vec[i]);
            dp[i+2] = max(dp[i+2], dp[i] + 2 * vec[i]);
        }
        cout << max(dp[n], dp[n+1]) << '\n';
    }
}