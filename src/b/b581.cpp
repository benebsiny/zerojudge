#include <iostream>
#include <sstream>
using namespace std;
int main(){
    int count, num, sum;
    cin >> count >> ws;
    string str;
    stringstream ss;
    while(count--){
        getline(cin, str);
        ss << str;
        sum = 0;
        while(ss >> num){
            sum += num;
        }
        cout << sum << '\n';
        ss.clear();
    }
    return 0;
}