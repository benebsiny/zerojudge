#include <iostream>
using namespace std;
int main(){
    int count, num;
    long long dp[751] = {1};
    for(int i=1; i<=750; i+=2){
        for(int j=i; j<=750; j++){
            dp[j] += dp[j-i];
        }
    }
    while(cin >> count){
        while(count--){
            cin >> num;
            cout << dp[num] << '\n';
        }
    }
    return 0;
}