#include <iostream>
#include <sstream>
#include <cstring>
using namespace std;

bool matrix[81][81];
bool visited[81];
bool c[81];

void DFS(int num){
    for(int i=0; i<=80; i++){
        if(matrix[num][i] && !visited[i]){
            visited[i] = true;
            DFS(i);
        }
    }
}

int main(){
    int count;
    stringstream ss;
    string input;
    int num1, num2, i, v, p;

    cin >> count >> ws;
    while(count--){
        memset(matrix, false, sizeof(matrix));
        memset(c, false, sizeof(c));
        memset(visited, false, sizeof(visited));

        v = p = 0;
        getline(cin, input);
        ss << input;
        while(ss >> input){
            num1 = num2 = 0;
            p++;
            for(i=0; input[i]!=','; i++){
                num1 *= 10;
                num1 += input[i] - '0';
            }
            for(i++; i<input.length(); i++){
                num2 *= 10;
                num2 += input[i] - '0';
            }
            // An array to check if a number exists
            if(!c[num1]){
                c[num1] = true;
                v++;
            }
            if(!c[num2]){
                c[num2] = true;
                v++;
            }

            matrix[num1][num2] = matrix[num2][num1] = true;
        }

        if(p!=v-1){
            cout << "F\n";
        }
        else {
            // start DFS
            for(i=0; i<=80; i++){
                if(c[i]){
                    visited[i] = true;
                    DFS(i);
                    break;
                }
            }

            // check if a tree
            bool notTree = false;
            for(int i=0; i<=80; i++){
                if(c[i] && !visited[i]){
                    notTree = true;
                    break;
                }
            }
            cout << (notTree ? "F" : "T") << '\n';
        }

        ss.clear();
    }
    return 0;
}
