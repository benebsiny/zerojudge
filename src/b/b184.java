package b;

import java.util.Scanner;

public class b184 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        while (sca.hasNext()){
            int count = sca.nextInt();
            int[] benefit = new int[101];
            while(count--!=0){
                int v = sca.nextInt();
                int b = sca.nextInt();
                for(int i=100; i>=v; i--){
                    benefit[i] = Math.max(benefit[i], benefit[i-v] + b);
                }
            }
            System.out.println(benefit[100]);
        }
    }
}
