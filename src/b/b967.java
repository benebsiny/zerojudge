package b;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Stack;

public class b967 {
    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(
                new InputStreamReader(System.in));

        String str;
        int parent, child;
        int count;
        while ((str = bf.readLine()) != null) {
            count = Integer.parseInt(str);
            LinkedList<Integer>[] list = new LinkedList[count];

            for (int i = 0; i < count - 1; i++) {
                str = bf.readLine();
                parent = Integer.parseInt(str.split(" ")[0]);
                child = Integer.parseInt(str.split(" ")[1]);
                if (list[parent] == null) {
                    list[parent] = new LinkedList<>();
                }
                list[parent].offer(child);
            }
            // Traversal
            Stack<Integer> stack = new Stack<>();

            boolean[] visited = new boolean[count];
            int[] deep = new int[count];
            int cur, par;
            int max = 0;
            for (int i = 0; i < count; i++) {
                while (!visited[i] && list[i] != null && !list[i].isEmpty()) {
                    stack.push(i);
                    while (!stack.isEmpty() && !visited[i]) {
                        if (list[i] != null && !list[i].isEmpty()) {
                            int a = list[i].pollFirst();
                            stack.push(a);
                            i = a;
                        } else if (list[i] == null) { // leaf
                            cur = stack.pop(); // ==i
                            par = stack.peek();
                            deep[par] = Math.max(deep[cur] + 1, deep[par]);
//                            int m = Math.max(deep[cur] + 1, deep[par]);
//                            if (max < deep[par] + m) {
//                                max = deep[par] + m;
//                            }
//                            deep[par] = m;

                            visited[cur] = true;
                            i = par;
                        } else { // Go through all child
                            if (stack.size() == 1) {
                                stack.clear();
                                break;
                            }
                            cur = stack.pop();
                            par = stack.peek();
                            int m = Math.max(deep[cur] + 1, deep[par]);
                            if (max < deep[par] + m) {
                                max = deep[par] + m;
                            }
                            deep[par] = m;

                            visited[cur] = true;
                            i = par;
                        }
                    }
                    if (visited[i] && stack.size() != 1) {
                        stack.pop();
                        int r = stack.pop();
                        visited[r] = true;
                        i = r;
                    }
                    visited[i] = true;
                }
            }
            System.out.println(max);
        }
    }
}
