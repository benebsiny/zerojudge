package e;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class e529 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        while(count--!=0) {
            sca.nextLine();
            sca.nextLine();
            List<Bind> list = new ArrayList<>();
            String[] nums = sca.nextLine().split(" ");
            for (String num : nums) {
                list.add(new Bind(Integer.parseInt(num)));
            }
            for (int i = 0; i < nums.length; i++) {
                list.get(i).val = sca.next();
            }
            list.sort(Comparator.comparingInt(b -> b.index));
            list.forEach(b-> System.out.println(b.val));
        }
    }
    static class Bind{
        Bind(int index){
            this.index = index;
        }
        int index;
        String val;
    }
}