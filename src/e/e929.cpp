#include <iostream>
#include <vector>
#include <map>
using namespace std;
int main(){
    vector<int> prime;
    map<int, int> factor;
    map<int, int>::iterator it;

    bool notPrime[4473] = {};
    notPrime[0] = true;
    notPrime[1] = true;
    for(int i=2; i<4473; i++){
        if(!notPrime[i]){
            prime.push_back(i);
            for(int j=i*2; j<4473; j+=i){
                notPrime[j] = true;
            }
        }
    }

    int num, num1;
    cin >> num;
    num1 = num;
    for(int i=0; i<prime.size(); i++){
        while(num%prime[i]==0){
            if(factor.find(prime[i])!=factor.end()){
                factor[prime[i]] = factor[prime[i]] + 1;
            } else {
                factor[prime[i]] = 1;
            }
            num /= prime[i];
        }
    }

    if(num!=1) factor[num] = 1;
    bool mul = false;

    cout << num1 << " = ";
    for(const auto& kv:factor){
        if(mul){
            cout << " * ";
        }
        if(kv.second==1){
            cout << kv.first;
        } else {
            cout << kv.first << "^" << kv.second;
        }
        mul = true;
    }

    return 0;
}