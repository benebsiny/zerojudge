#include <iostream>
#include <set>
#include <vector>
using namespace std;
int main(){
    int count, a, b, rest, i;
    cin >> count;
    while(count--){
        vector<int> re;
        vector<int> ans;
        cin >> a >> b;
        cout << (a / b) << ".";
        rest = a % b;
        re.push_back(rest);

        bool repeat = false;
        int digit = 0;
        while(rest && digit < 50){
            rest *= 10;
            
            ans.push_back(rest / b);
            rest %= b;

            for(i=0; i<re.size(); i++){
                if(re[i] == rest){
                    repeat = true;
                    break;
                }
            }
            if(repeat) break;
            re.push_back(rest);
            digit++;
        }
        if(repeat){
            for(int x=0; x<i; x++){
                cout << ans[x];
            }
            cout << "(";
            for(int x=i; x<ans.size(); x++){
                cout << ans[x];
            }
            cout << ")";
        } else if(digit >= 50) {
            cout << "(";
            for(int a:ans){
                cout << a;
            }
            cout << "...)";
        } else {
            for(int a:ans){
                cout << a;
            }
            cout << "(0)";
        }
        cout << "\n";
    }
}
