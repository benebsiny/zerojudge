#include <stdio.h>
#include <math.h>
int main(){
    double T, n;
    while(~scanf("%lf%lf", &T, &n)){
        printf("%.3lf\n", round((-T*log2(n))*1000)/1000);
    }
    return 0;
}