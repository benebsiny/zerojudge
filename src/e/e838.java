package e;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class e838 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);

        int count = sca.nextInt();
        Set<Integer> rowSet = new HashSet<>();
        Set<Integer> colSet = new HashSet<>();

        sca.nextLine();
        for (int i = 0; i < count; i++) {
            String str = sca.nextLine();
            if (str.indexOf('*') != -1) {
                rowSet.add(i);
                for (int j = 0; j < str.length(); j++) {
                    if (str.charAt(j) == '*') {
                        colSet.add(j);
                    }
                }
            }
        }
        char[][] map = new char[count][count];
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                map[i][j] = '0';
            }
        }

        for (int i : rowSet) {
            for (int j = 0; j < count; j++) {
                map[i][j] = '*';
            }
        }
        for (int j : colSet) {
            for (int i = 0; i < count; i++) {
                map[i][j] = '*';
            }
        }
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }
}
