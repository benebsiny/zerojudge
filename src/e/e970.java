package e;

import java.util.Scanner;

public class e970 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        int[] arr = new int[count+1];
        for (int i = 1; i <= count; i++) {
            arr[i] = sca.nextInt();
        }
        int sum = 0;
        for (int i = 1; i <= count; i++) {
            if (i % arr[count] == 1) {
                sum += arr[i];
            }
        }
        int n = sum % count;
        if (n == 0) {
            System.out.println(count + " " +arr[count]);
        }
        else {
            System.out.println(n + " " +arr[n]);
        }
    }
}
