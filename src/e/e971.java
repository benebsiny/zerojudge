package e;

import java.util.Scanner;

public class e971 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int row = sca.nextInt();
        int col = sca.nextInt();
        int[][] arr = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                arr[i][j] = sca.nextInt();
            }
        }
        for (int i = 0; i < row; i++) {
            int index = -1;
            for (int j = 0; j < col; j++) {
                if (arr[i][j] == 1) {
                    if (index == -1) {
                        index = j;
                    } else {
                        for (int x = index; x <= j; x++) {
                            arr[i][x] = 1;
                        }
                        index = -1;
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
