package e;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class e901u {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        while (true) {
            int count = sca.nextInt();
            if (count == 0) break;
            Integer[] arr1 = new Integer[count];
            Integer[] arr2 = new Integer[count];
            for (int i = 0; i < count; i++) {
                arr1[i] = sca.nextInt();
            }
            for (int i = 0; i < count; i++) {
                arr2[i] = sca.nextInt();
            }
            Arrays.sort(arr1, Collections.reverseOrder());
            Arrays.sort(arr2);

            int res = 0;
            for (int i = 0, j = 0; j < count; j++) {
                if (arr1[i] + arr2[j] > 10) {
                    res++;
                    i++;
                }
            }
            System.out.println(res + " " + res * 200);
        }
    }
}
