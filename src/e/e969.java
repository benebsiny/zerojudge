package e;

import java.util.Scanner;

public class e969 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int money = sca.nextInt();
        int gap = sca.nextInt();
        int current = sca.nextInt();
        int[] moneys = {32, 55};

        if(money - moneys[current] < 0){
            System.out.println("Wayne can't eat and drink.");
            System.exit(0);
        }

        int time = 0;
        // 0: apple, 1:soup
        while (money > 0) {
            if (money - moneys[current] >= 0) {
                money -= moneys[current];
                if (current == 0) {
                    System.out.print(time + ": Wayne eats an Apple pie");
                } else {
                    System.out.print(time + ": Wayne drinks a Corn soup");
                }

                if (money == 0) {
                    System.out.print(", and now he doesn't have money.");
                } else {
                    System.out.print(", and now he has " + money + " dollar" + ((money != 1) ? "s" : "") + ".");
                }
            }
            else {
                break;
            }
            current = (current - 1) * (-1);
            time += gap;
            System.out.println();
        }

    }
}
