#include <iostream>
#include <algorithm>
using namespace std;
int main(){
    int counta, countb;

    cin >> counta;
    int a[counta+1];
    for(int i=0; i<counta+1; i++){
        cin >> a[i];
    }

    cin >> countb;
    int b[countb+1];
    for(int i=0; i<countb+1; i++){
        cin >> b[i];
    }
    reverse(a, a+counta+1);
    reverse(b, b+countb+1);

    int res[counta+countb+1] = {};

    for(int i=0; i<counta+1; i++){
        if(a[i]==0) continue;
        for(int j=0; j<countb+1; j++){
            res[i+j] += a[i]*b[j];
        }
    }

    reverse(res, res+(counta + countb) + 1);

    cout << counta + countb << endl;
    for(int i=0; i<counta + countb + 1; i++){
        cout << res[i] << " ";
    }
    return 0;
}