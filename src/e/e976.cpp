#include <iostream>
using namespace std;
int main(){
    int h, m, s;
    while(cin >> h >> m >> s){
        cout << h << " " << m << " " << s << ". ";
        if(m/(double)s<=h){
            cout << "I will make it!\n";
        } else {
            cout << "I will be late!\n";
        }
    }
    return 0;
}