package e;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class e577 {

    static boolean[] notPrime = new boolean[46341];
    static List<Integer> prime = new ArrayList<>();

    public static void main(String[] args) {

        init();

        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        for (int i = 0; i < count; i++) {
            int num = sca.nextInt() + 1;
            while (true) {
                if (!isPrime(num)) {
                    if (isSmith(num)) {
                        System.out.println(num);
                        break;
                    }
                }
                num++;
            }
        }
    }

    static void init() {
        notPrime[0] = true;
        notPrime[1] = true;

        for (int i = 2; i < notPrime.length; i++) {
            if (!notPrime[i]) {
                prime.add(i);
                for (int j = 2; i * j < notPrime.length; j++) {
                    notPrime[i * j] = true;
                }
            }
        }
    }

    static boolean isPrime(int num) {
        if (num < notPrime.length) {
            return !notPrime[num];
        } else {
            for (int p : prime)
                if (p * p <= num && num % p == 0)
                    return false;
            return true;
        }
    }

    static boolean isSmith(int num) {
        int num1 = num;
        List<Integer> factor = new ArrayList<>();
        for (int p : prime)
            if (p * p <= num && num % p == 0)
                while (num % p == 0) {
                    factor.add(p);
                    num /= p;
                }
        if (num != 1) {
            factor.add(num);
        }

        int sum1 = sumDigit(num1);
        int sum2 = 0;
        for (int f : factor) {
            sum2 += sumDigit(f);
        }

        return sum1 == sum2;
    }

    private static int sumDigit(int num) {
        int sum = 0;
        while (num != 0) {
            sum += num % 10;
            num /= 10;
        }
        return sum;
    }
}
