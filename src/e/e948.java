package e;

import java.util.Scanner;

public class e948 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        for (int i = 0; i < count; i++) {
            int g = sca.nextInt();
            int a = sca.nextInt();
            int h = sca.nextInt();
            int w = sca.nextInt();

            double bmr;
            if (g == 1) { // Male
                bmr = 13.7 * w + 5.0 * h - 6.8 * a + 66;
            }
            else { // Female
                bmr = 9.6 * w + 1.8 * h - 4.7 * a + 655;
            }
            bmr *= 100;
            bmr = Math.round(bmr);
            bmr /= 100;
            System.out.printf("%.2f%n", bmr);
        }
    }
}
