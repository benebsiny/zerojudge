package e;

import java.util.Scanner;

public class e913 {
  public static void main(String[] args) {
    boolean[] notPrime = new boolean[123457];
    notPrime[0] = notPrime[1] = true;
    notPrime[2] = false;
    for(int i=2; i<123457; i++){
      if(!notPrime[i]){
        for(int j=i*2; j<123457; j+=i){
          notPrime[j] = true;
        }
      }
    }
    Scanner sca = new Scanner(System.in);
    int num = sca.nextInt(), count = 0;
    for(int i=num; i>=2; i--){
      if(!notPrime[i] && !notPrime[i-2]){
        count++;

      }
    }
    System.out.println(count);
  }
}
