#include <iostream>
#include <queue>
#define MAX 1000001
typedef long long ll;
using namespace std;

ll nums[MAX];
queue<ll> q;

int main(){
    for(int i=1; i<10; i++){
        q.push(i);
    }

    int input;
    cin >> input;

    int index = 1;
    while(nums[input] == 0){
        ll num = q.front();
        q.pop();
        ll mod = num % 10;

        if(mod != 0) q.push(num * 10 + mod - 1);
        q.push(num * 10 + mod);
        if(mod != 9) q.push(num * 10 + mod + 1);

        nums[index] = num;
        index++;
    }

    cout << nums[input];

    return 0;
}