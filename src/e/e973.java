package e;

import java.util.Arrays;
import java.util.Scanner;

public class e973 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        String str = sca.next();

        Com[] arr = new Com[11];
        for (int i = 0; i <= 10; i++) {
            arr[i] = new Com(i);
        }
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i) - '0'].count++;
        }
        Arrays.sort(arr, (o1, o2) -> {
            if (o1.count > o2.count) {
                return -1;
            } else if (o1.count < o2.count) {
                return 1;
            } else {
                return Integer.compare(o1.num, o2.num);
            }
        });
        for (Com com : arr) {
            if (com.count > 0) {
                System.out.print(com.num + " ");
            }
        }
    }

    static class Com {
        int num;
        int count;

        public Com(int num) {
            this.num = num;
        }
    }
}
