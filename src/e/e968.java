package e;

import java.util.Scanner;

public class e968 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        int[] stu = new int[count+1];
        stu[sca.nextInt()]++;
        stu[sca.nextInt()]++;
        stu[sca.nextInt()]++;
        for(int i=count; i>0; i--){
            if(stu[i]==0){
                System.out.println("No. " + i);
            }
        }
    }
}
