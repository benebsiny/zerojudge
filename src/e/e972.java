package e;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class e972 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("T", "1.0");
        map.put("U", "30.9");
        map.put("J", "0.28");
        map.put("E", "34.5");

        Scanner sca = new Scanner(System.in);
        BigDecimal total = new BigDecimal(sca.next());
        BigDecimal cost = new BigDecimal(sca.next());
        String currency = sca.next();

        BigDecimal rest = total.subtract(cost.multiply(new BigDecimal(map.get(currency)))).divide(new BigDecimal(map.get(currency)), 2, BigDecimal.ROUND_HALF_UP);

        if (rest.compareTo(new BigDecimal("0")) == 0) {
            System.out.printf("%s %.2f\n", currency, rest.doubleValue());
        } else if (rest.compareTo(new BigDecimal("0")) > 0 && rest.compareTo(new BigDecimal("0.05")) < 0) {
            System.out.printf("%s %.2f\n", currency, 0.00);
        } else if (rest.compareTo(new BigDecimal("0")) < 0) {
            System.out.println("No Money");
        } else {
            System.out.printf("%s %.2f\n", currency, rest.doubleValue());
        }
    }
}
