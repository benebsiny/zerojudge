package e;

import java.util.Scanner;

public class e974 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int row = sca.nextInt();
        int col = sca.nextInt();
        int wks = sca.nextInt();

        int[][] arr = new int[row][col];
        for (int i = 0, k = 1; i < row; i++) {
            for (int j = 0; j < col; j++, k++) {
                arr[i][j] = k;
            }
        }
        for (int w = 2; w <= wks; w++) {
            if (w % 2 == 0) { //even, right
                for (int i = 0; i < row; i++) {
                    int last = arr[i][col - 1];
                    System.arraycopy(arr[i], 0, arr[i], 1, col - 1);
                    arr[i][0] = last;
                }
            } else {
                for (int j = 0; j < col; j++) {
                    int last = arr[row - 1][j];
                    for (int i = row - 1; i >= 1; i--) {
                        arr[i][j] = arr[i - 1][j];
                    }
                    arr[0][j] = last;
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
