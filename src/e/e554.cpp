#include <iostream>
#include <string>
using namespace std;

bool isVowel(char ch){
    return ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u';
}

int main(){
    int count;
    string str1, str2;
    cin >> count;
    for(int i=0; i<count; i++){
        cin >> str1;
        cin >> str2;
        if(str1.length() != str2.length()){
            cout << "No" << endl;
        } else {
            int i;
            for(i=0; i<str1.length(); i++){
                if(isVowel(str1[i])){
                    if(!isVowel(str2[i])){
                        cout << "No" << endl;
                        break;
                    }
                } else {
                    if(str1[i]!=str2[i]){
                        cout << "No" << endl;
                        break;
                    }
                }
            }
            if(i>=str1.length()){
                cout << "Yes" << endl;
            }
        }
    }
    return 0;
}