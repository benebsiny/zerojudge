package e;

import java.util.Scanner;

public class e914 {
  public static void main(String[] args) {
    Scanner sca = new Scanner(System.in);
    int yearsLater = sca.nextInt(), mom;
    boolean exist = false;
    for(int kid=1; kid<=99; kid++){
      mom = 10*(kid%10)+(kid/10);
      if(kid>=mom) continue;
      if(2*(kid+yearsLater) == (mom+yearsLater) && (18<=mom && mom+yearsLater<=99)){
        System.out.println(kid);
        exist = true;
        break;
      }
    }
    if(!exist){
      System.out.println("no answer");
    }
  }
}
