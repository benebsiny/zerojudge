#include <stdio.h>
#include <algorithm>
#include <functional>

using namespace std;

int main(){
    int count;
    while(true){
        scanf("%d", &count);
        if(count==0) break;

        int arr1[count];
        int arr2[count];

        for(int i=0; i<count; i++){
            scanf("%d", &arr1[i]);
        }
        for(int i=0; i<count; i++){
            scanf("%d", &arr2[i]);
        }
        sort(arr1, arr1+count, greater<int>());
        sort(arr2, arr2+count);

        int res = 0;
        for (int i = 0, j = 0; j < count; j++) {
            if (arr1[i] + arr2[j] > 10) {
                res++;
                i++;
            }
        }
        printf("%d %d\n", res, res*200);

    }
    return 0;
}