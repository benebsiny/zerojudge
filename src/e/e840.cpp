#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

bool hasDigit(string pswd){
    return find_if(pswd.begin(), pswd.end(), ::isdigit)!=pswd.end();
}

bool hasAlpha(string pswd){
    return find_if(pswd.begin(), pswd.end(), ::isalpha)!=pswd.end();
}

int main(){
    string pswd;
    cin >> pswd;
    int score = 0;
    if(pswd.length() > 8 && hasDigit(pswd) && hasAlpha(pswd)){
        score += 10;
    } else {
        score -= 5;
    }

    bool prevIsDigit = false;
    score += pswd.length() * 3;
    for (int i = 0; i < pswd.length(); i++)
    {
        if('0' <= pswd[i] && pswd[i]<= '9'){
            score += 2;

            if(prevIsDigit){
                score-=2;
            }
            prevIsDigit = true;
        } else {
            score += 3;
            prevIsDigit = false;
        }
    }

    if(!hasAlpha(pswd)) {
        score -= pswd.length();
    }
    if(!hasDigit(pswd)){
        score -= pswd.length();
    }
    

    cout << score;
    return 0;
}