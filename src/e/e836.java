package e;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class e836 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        Map<Integer, Integer> map = new LinkedHashMap<>();

        int max = 1;
        for (int i = 0; i < count; i++) {
            int num = sca.nextInt();
            if (map.containsKey(num)) {
                int updated = map.get(num) + 1;
                map.put(num, updated);
                if (updated > max) {
                    max = updated;
                }
            } else {
                map.put(num, 1);
            }
        }
        System.out.println(map.keySet().size());
        if (max == 1) {
            System.out.println("NO");
        } else {
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getValue() == max) {
                    System.out.print(entry.getKey() + " ");
                }
            }
        }
    }
}
