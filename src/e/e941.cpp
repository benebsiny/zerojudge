#include <stdio.h>
#include <deque>
using namespace std;

int GCD(int a, int b) {
	if (b) while((a %= b) && (b %= a));
	return a + b;
}

int main(){
    deque<int> kid;
    deque<int> parent;

    int a, b;
    scanf("%d/%d", &a, &b);
    kid.push_back(a);
    parent.push_back(b);

    char ch[5][2];
    int ind = 0;
    while(~scanf("%s %d/%d", ch[ind++], &a, &b)){
        kid.push_back(a);
        parent.push_back(b);
    }

    int ind2 = 0;
    a = kid.front();
    b = parent.front();
    kid.pop_front();
    parent.pop_front();
    int c, d, gcd, lcm;
    while(!kid.empty()){
        c = kid.front();
        d = parent.front();

        gcd = GCD(b, d);
        lcm = b * d / gcd;
        a *= lcm / b;
        c *= lcm / d;
        b = d = lcm;
        if(ch[ind2][0]=='+'){
            a += c;
        } else {
            a -= c;
        }

        gcd = GCD(a, b);
        a /= gcd;
        b /= gcd;

        ind2++;
        kid.pop_front();
        parent.pop_front();
    }

    gcd = GCD(a, b);
    a /= gcd;
    b /= gcd;

    if(a==0) printf("0/1");
    else if(a<0 && b<0) printf("%d/%d", -a, -b);
    else if(b<0) printf("%d/%d", -a, -b);
    else printf("%d/%d", a, b);
    return 0;
}