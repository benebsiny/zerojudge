package e;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class e925 {
  public static void main(String[] args) {
    Scanner sca = new Scanner(System.in);
    int count = sca.nextInt();
    String id;
    List<String> list = new ArrayList<>();
    for(int i=0; i<count; i++){
      list.add(sca.next());
    }
    int pass = 0;
    boolean isPass;
    for(int i=0; i<10; i++){
      isPass = false;
      id = sca.next();
      if(id.charAt(0)=='B'){
        if(id.substring(1, 3).matches("[\\d]{2}")){
          if(list.contains(id.substring(3, 7))){
            if(id.substring(7, 9).matches("[\\d]{2}")){
              System.out.println("Y");
              pass++;
              isPass = true;
            }
          }
        }
      }
      if(!isPass)
        System.out.println("N");
    }
    if(pass!=10)
      System.out.println((10-pass)/10.0);
    else
      System.out.println(0);
  }
}
