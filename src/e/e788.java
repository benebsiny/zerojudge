package e;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class e788 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();

        Stu[] stus = new Stu[count];
        for (int i = 0; i < count; i++) {
            stus[i] = new Stu(sca.next(), sca.next());
        }

        // 自訂排序
        Arrays.sort(stus, (o1, o2) -> {
            if (o1.id.charAt(o1.id.length() - 1) != o2.id.charAt(o2.id.length() - 1)) {
                return o1.id.charAt(o1.id.length() - 1) - o2.id.charAt(o2.id.length() - 1);
            }
            else if(o1.id.charAt(0) != o2.id.charAt(0)) {
                return o1.id.charAt(0) - o2.id.charAt(0);
            }
            return 0;
        });

        for(Stu stu: stus){
            System.out.println(stu.id.charAt(stu.id.length()-1) + ": " + stu.name);
        }

    }

    static class Stu {
        public String id;
        public String name;

        public Stu(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
