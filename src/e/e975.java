package e;

import java.util.Scanner;

public class e975 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        String str = sca.nextLine();

        str = str.toLowerCase();

        boolean[] order = new boolean[2];
        for (int i = 1; i < str.length(); i++) {
            int dist = str.charAt(i) - str.charAt(i - 1);
            if (dist < 0) dist += 26;

            if (dist == 3) {
                order[0] = true;
            } else if (order[0] && dist == 7) {
                order[1] = true;
            } else if (order[0] && order[1] && dist == 9) {
                System.out.println(('e' - str.charAt(i) + 26) % 26);
                break;
            } else {
                order[0] = order[1] = false;
            }
        }
    }
}
