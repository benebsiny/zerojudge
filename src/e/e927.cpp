#include <iostream>
using namespace std;
int main(){
    ios::sync_with_stdio(false);
    char ch;
    int arr[26] = {};
    while((ch = getchar()) != EOF){
        arr[ch-'A']++;
    }
    for(int i=0; i<26; i++){
        for(int j=0; j<arr[i]; j++){
            cout << (char)(i+'A');
        }
    }
    return 0;
}