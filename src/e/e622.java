package e;

import java.util.Scanner;

public class e622 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        int count = sca.nextInt();
        int stardust = sca.nextInt();
        int picked = 0, afterEvolve = 0;

        int evolveCount = stardust / 1000;

        int cp, iv;
        for (int i = 0; i < count; i++) {
            cp = sca.nextInt();
            iv = sca.nextInt();

            if (0 <= iv && iv <= 29) {
                cp += evolveCount * 10;
            } else if (30 <= iv && iv <= 39) {
                cp += evolveCount * 50;
            } else if (40 <= iv && iv <= 45) {
                cp += evolveCount * 100;
            }

            if (cp > afterEvolve) {
                afterEvolve = cp;
                picked = i;
            }
        }

        System.out.println(picked + 1 + " " + afterEvolve);
    }
}
